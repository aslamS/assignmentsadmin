package com.testng;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DemoTesting {
	public static WebDriver driver;

	@BeforeClass
	public void openBrowser() {
		driver = new ChromeDriver();
		driver.get("https://admin-demo.nopcommerce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
	}

	@Test(priority = 1)
	public void Login() {
		// driver.findElement(By.id("Email")).sendKeys("admin@yourstore.com");
		// driver.findElement(By.id("Password")).sendKeys("admin");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String actual = driver.findElement(By.partialLinkText("John Smith")).getText();
		System.out.println(actual);
		String expt = "John Smith";
		Assert.assertEquals(actual, expt);
		driver.findElement(By.xpath("(//*[@class='nav-link'])[4]")).click();
	}

	@Test(priority = 2)
	public void category() throws InterruptedException, IOException {
		File f = new File("/home/aslam/Downloads/Exceldata/exceldata.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sh = wb.getSheetAt(0);
		int rows = sh.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
			driver.findElement(By.xpath("//p[contains(text(),' Categories')]")).click();
			driver.findElement(By.xpath("//i[@class='fas fa-plus-square']")).click();
			String name = sh.getRow(i).getCell(0).getStringCellValue();
			String description = sh.getRow(i).getCell(0).getStringCellValue();
			String cvalue = sh.getRow(i).getCell(1).getStringCellValue();
			driver.findElement(By.id("Name")).sendKeys(name);

			driver.findElement(By.id("Description_ifr")).sendKeys(description);
			WebElement catalog = driver.findElement(By.id("ParentCategoryId"));
			Select s = new Select(catalog);
			s.selectByVisibleText("Computers >> Build your own computer");
			WebElement price = driver.findElement(By.xpath("//label[@for=\"PriceFrom\"]"));

			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView()", price);

		}

	}

	@Test(priority = 3)
	public void Products() {
		driver.findElement(By.partialLinkText("Products")).click();
		driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
		WebElement dd = driver.findElement(By.id("SearchCategoryId"));
		Select s = new Select(dd);
		s.selectByIndex(2);
		driver.findElement(By.id("search-products")).click();
	}

	@Test(priority = 4)
	public void Manufactures() throws IOException {
		driver.findElement(By.partialLinkText("Manufacturers")).click();
		driver.findElement(By.xpath("//a[@class='btn btn-primary']")).click();

		File f = new File("/home/aslam/Downloads/Exceldata/exceldata.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sh = wb.getSheetAt(1);
		int rows = sh.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {

			String srh = sh.getRow(i).getCell(0).getStringCellValue();
			String pub = sh.getRow(i).getCell(0).getStringCellValue();

			driver.findElement(By.id("SearchManufacturerName")).sendKeys(srh);

			driver.findElement(By.id("SearchPublishedId")).sendKeys(pub);

			driver.findElement(By.xpath("//button[@name='save']")).click();

		}

	}

	@Test(priority = 5)
	public void Logoutfun() {
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
	}

	@AfterClass
	public void closebrowser() {
		driver.quit();
	}

}
